# ActivityWatch Backup Scripts

Various scripts to backup [ActivityWatch](https://github.com/ActivityWatch/activitywatch) buckets and categories.

## Scripts

- `backup.py` - Python script to backup all buckets to individual JSON files or one big single JSON file.
- `category-backup.js` - JavaScript snippet that can be pasted in a browser console to export web UI categories since they are only stored in the browser. This will be useless once ActivityWatch supports storing categories in the backend. See <https://github.com/ActivityWatch/activitywatch/issues/348>.
