#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
from urllib.request import urlopen
import cgi
import json
import argparse

parser = argparse.ArgumentParser(description='Backs up ActivityWatch buckets.',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('dir', action='store', type=str, default="./",
                    help='Path to export buckets to', nargs="?")
parser.add_argument('-u', '--url', dest='url', action='store',
                    type=str, default="http://localhost", help='address of ActivityWatch instance')
parser.add_argument('-p', '--port', dest='port', action='store',
                    type=int, default=5600, help='specify a custom ActivityWatch port')
parser.add_argument('--combined', dest='combined', action='store_true',
                    default=False, help='export to a single large JSON file')
args = parser.parse_args()

api_url = "{0}:{1}".format(args.url, args.port)


def get_json(url):
    try:
        with urlopen(url) as response:
            data = json.load(response)
            return data
    except:
        print(
            "[ActivityWatch Backup] Warning: This script needs ActivityWatch to be running to work.")
        raise


if args.combined:
    url = "{0}/api/0/export".format(api_url)
    with urlopen(url) as response:
        _, params = cgi.parse_header(
            response.headers.get('Content-Disposition', ''))
        filename = os.path.join(args.dir, params['filename'])

        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, "w") as bucket_file:
            bucket_file.write(response.read().decode())
else:
    buckets = get_json("{0}/api/0/buckets/".format(api_url))
    print("Discovered {0} bucket(s)".format(len(buckets)))
    for bucket in buckets:
        url = "{0}/api/0/buckets/{1}/export".format(api_url, bucket)
        with urlopen(url) as bucket_json:
            filename = os.path.join(args.dir, "{0}.json".format(bucket))
            os.makedirs(os.path.dirname(filename), exist_ok=True)
            with open(filename, "w") as bucket_file:
                bucket_file.write(bucket_json.read().decode())
                print("Exported {0} bucket".format(bucket))

print(
    "Successfully exported all ActivityWatch buckets to {0}".format(args.dir))
print("\nPlease note, categories cannot be backed by this script as they are stored in the browser.")
