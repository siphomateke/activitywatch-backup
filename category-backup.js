(function () {
  var blob = new Blob([window.localStorage.classes], { type: 'text/json' }),
    a = document.createElement('a');
  a.download = 'aw-categories.json';
  a.href = window.URL.createObjectURL(blob);
  a.dataset.downloadurl = ['text/json', a.download, a.href].join(':');
  a.click();
})(); 
